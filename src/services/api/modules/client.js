import * as R from 'ramda'
import axios from 'axios'

const apiClient = axios.create({
  baseURL: process.env.API_HOST,
  timeout: 30000
})

apiClient.interceptors.response.use(
  response => response.data,
  error => {
    throw R.pathOr(
      { code: 'UNKNOWN_SERVER_ERROR', message: 'Unknown server error.' },
      ['response', 'data'],
      error
    )
  }
)

export default apiClient
