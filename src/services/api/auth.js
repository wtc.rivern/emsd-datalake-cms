import apiClient from './modules/client'
import * as paths from './modules/paths'

export const login = (username, password) =>
  apiClient.post(process.env.LOGIN, {
    username,
    password
  })

export const refresh = refreshToken =>
  apiClient.post(paths.refresh, {
    refreshToken
  })
