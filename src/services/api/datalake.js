import apiClient from './modules/client'

export const createLabellingJob = (mode, accessToken) => {
  const headers = {
    headers: {
      Authorization: `Bearer ${accessToken}`
    }
  }
  if (mode === 'detection') {
    return apiClient.post(process.env.CREATE_LABELLING_JOB+'?mode=detection', {}, headers)
  } else {
    return apiClient.post(process.env.CREATE_LABELLING_JOB, {}, headers)
  }
}

export const checkLabellingJobStatus = (accessToken, labellingJobName) => {
  const headers = {
    headers: {
      Authorization: `Bearer ${accessToken}`
    }
  }
  return apiClient.post(process.env.CHECK_LABELLING_JOB_STATUS, {labellingJobName: labellingJobName}, headers)
}

export const startTraining = (mode, accessToken, labellingJobName) => {
  const headers = {
    headers: {
      Authorization: `Bearer ${accessToken}`
    }
  }
  if (mode === 'detection') {
    return apiClient.post(process.env.START_TRAINING+'?mode=detection', {labellingJobName: labellingJobName}, headers)
  } else {
    return apiClient.post(process.env.START_TRAINING, {labellingJobName: labellingJobName}, headers)
  }
}

export const checkTrainingJobStatus = (mode, accessToken) => {
  const headers = {
    headers: {
      Authorization: `Bearer ${accessToken}`
    }
  }
  if (mode === 'detection') {
    return apiClient.post(process.env.CHECK_TRAINING_JOB_STATUS+'?mode=detection', {}, headers)
  } else {
    return apiClient.post(process.env.CHECK_TRAINING_JOB_STATUS, {}, headers)
  }
}

export const createModel = (mode, accessToken) => {
  const headers = {
    headers: {
      Authorization: `Bearer ${accessToken}`
    }
  }
  if (mode === 'detection') {
    return apiClient.post(process.env.CREATE_MODEL+'?mode=detection', {}, headers)
  } else {
    return apiClient.post(process.env.CREATE_MODEL, {}, headers)
  }
}

export const checkEndpointStatus = (mode, accessToken) => {
  const headers = {
    headers: {
      Authorization: `Bearer ${accessToken}`
    }
  }
  if (mode === 'detection') {
    return apiClient.post(process.env.CHECK_ENDPOINT_STATUS+'?mode=detection', {}, headers)
  } else {
    return apiClient.post(process.env.CHECK_ENDPOINT_STATUS, {}, headers)
  }
}

export const invokeEndpoint = (mode, accessToken, image) => {
  const formData = new window.FormData()
  formData.append('file', image)
  const headers = {
    headers: {
      Authorization: `Bearer ${accessToken}`,
      'content-type': 'multipart/form-data'
    }
  }
  if (mode === 'demo') {
    return apiClient.post(process.env.DEMO_INVOKE_ENDPOINT, formData, headers)
  } else if (mode === 'detection') {
    return apiClient.post(process.env.INVOKE_ENDPOINT+'?mode=detection', formData, headers)
  } else {
    return apiClient.post(process.env.INVOKE_ENDPOINT, formData, headers)
  }
}
