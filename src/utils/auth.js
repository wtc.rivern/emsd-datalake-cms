import { ACCESS_TOKEN, REFRESH_TOKEN, USERNAME } from '../constants/tokenKeys'

export const storeTokens = ({ accessToken, refreshToken, username }) => {
  window.localStorage.setItem(ACCESS_TOKEN, accessToken)
  window.localStorage.setItem(REFRESH_TOKEN, refreshToken)
  window.localStorage.setItem(USERNAME, username)
}

export const removeTokens = () => {
  window.localStorage.removeItem(ACCESS_TOKEN)
  window.localStorage.removeItem(REFRESH_TOKEN)
  window.localStorage.removeItem(USERNAME)
}

export const getTokens = () => {
  if (process.browser) {
    const accessToken = window.localStorage.getItem(ACCESS_TOKEN)
    const refreshToken = window.localStorage.getItem(REFRESH_TOKEN)
    const username = window.localStorage.getItem(USERNAME)
    return {
      accessToken,
      refreshToken,
      username
    }
  } else {
    return {
      accessToken: null,
      refreshToken: null,
      username: null
    }
  }
}
