import NotFound from './pages/NotFound'
import Login from './pages/Login'
import Dashboard from './pages/Dashboard'

export const publicRoutes = [
  {
    path: '/login',
    component: Login
  },
  {
    path: '/dashboard',
    component: Dashboard
  },
  {
    path: '/404',
    component: NotFound
  }
]

export const restrictedRoutes = [
  {
    path: '/',
    component: Login
  },
]
