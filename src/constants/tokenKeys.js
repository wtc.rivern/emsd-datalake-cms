// the keys when storing tokens into localStorage
export const ACCESS_TOKEN = '_at'
export const REFRESH_TOKEN = '_rt'
export const USERNAME = '_un'
