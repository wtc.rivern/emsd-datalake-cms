import 'react-hot-loader'
import 'core-js/stable'
import 'regenerator-runtime/runtime'
import React, { Fragment } from 'react'
import ReactDom from 'react-dom'
import { Provider } from 'mobx-react'
import CssBaseline from '@material-ui/core/CssBaseline'

import RootStore from './stores/RootStore'
import './styles/main.scss'
import App from './App'

const rootStore = new RootStore()

ReactDom.render(
  <Fragment>
    <CssBaseline />
    <Provider {...rootStore}>
      <App />
    </Provider>
  </Fragment>,
  document.getElementById('app')
)
