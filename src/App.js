import { hot } from 'react-hot-loader/root'
import React from 'react'
import { Router } from 'react-router-dom'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'

import LoadingController from './components/layouts/LoadingController'
import history from './history'

const themeColor = '#2F639E'

const theme = createMuiTheme({
  typography: {
    useNextVariants: true
  },
  palette: {
    primary: {
      main: themeColor,
      contrastText: '#fff'
    },
    secondary: {
      main: '#BFBFBF',
      contrastText: '#fff'
    },
    text: {
      primary: '#575757'
    },
  },
  overrides: {
    MuiFormHelperText: {
      root: {
        fontSize: 14
      }
    },
    MuiTableCell: {
      head: {
        fontSize: 14
      }
    }
  }
})

class App extends React.Component {
  render () {
    return (
      <MuiThemeProvider theme={theme}>
        <Router history={history}>
          <LoadingController />
        </Router>
      </MuiThemeProvider>
    )
  }
}

export default hot(App)
