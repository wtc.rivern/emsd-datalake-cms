import React from 'react'
import { Switch, Route, Redirect } from 'react-router-dom'
import { inject, observer } from 'mobx-react'
import { withStyles } from '@material-ui/core'

import { restrictedRoutes } from '../../routes'

const styles = theme => ({
  root: {
    display: 'flex'
  },
  main: {
    flexGrow: 1,
    padding: theme.spacing(8)
  }
})

class Layout extends React.Component {
  render () {
    const { classes } = this.props

    return (
      <div className={classes.root}>
        <main className={classes.main}>
          <Switch>
            {restrictedRoutes.map(config => (
              <Route key={config.path} {...config} />
            ))}
            <Redirect to='/404' />
          </Switch>
        </main>
      </div>
    )
  }
}

export default withStyles(styles)(Layout)
