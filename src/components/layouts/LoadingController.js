import React from 'react'
import { inject, observer } from 'mobx-react'
import { Switch, Route } from 'react-router-dom'

import { publicRoutes } from '../../routes'
import LoadingPage from '../../pages/LoadingPage'
import Layout from './Layout'

class LoadingController extends React.Component {
  render () {
    return (
      <Switch>
        {publicRoutes.map(config => (
          <Route key={config.path} {...config} />
        ))}
        <Route path='/' component={Layout} />
      </Switch>
    )
  }
}

export default LoadingController
