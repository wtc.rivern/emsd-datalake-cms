import * as yup from 'yup'
import React from 'react'
import {
  Grid,
  Typography,
  Box,
  withStyles,
  Dialog,
  DialogContent,
  DialogActions,
  Button,
  FormControlLabel,
  Radio,
  RadioGroup,
} from '@material-ui/core'
import { styled } from '@material-ui/styles'
import { TextField } from 'formik-material-ui'
import { Formik, Form } from 'formik'
import { Timer } from 'react-compound-timer'

import menuIcon01 from '../../images/menu_icon_01.png'
import menuIcon02 from '../../images/menu_icon_02.png'
import menuIcon03 from '../../images/menu_icon_03.png'
import menuIcon04 from '../../images/menu_icon_04.png'
import processIcon from '../../images/process.gif'

import {
  Stage,
  Layer,
  Rect,
  Text,
  Circle,
  Line
} from "react-konva/lib/ReactKonvaCore"

import "konva/lib/shapes/Rect"
import "konva/lib/shapes/Text"
import "konva/lib/shapes/Circle"
import "konva/lib/shapes/Line"

const styles = {
  root: {
    padding: 30,
    marginTop: 30
  },
  button: {
    background: "#2FD5B1",
    height: 30,
    borderRadius: 0
  },
}

@withStyles(styles)

class Dashboard extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      mode: 'demo',
      open: false,
      dimensions: {
        w: 0,
        h: 0
      }
    }
  }

  handleClick = (event) => {
    const { isReadyToInvokeEndpoint } = this.props
    this.inputElement.click()
  }

  handleFileChange = (event) => {
    const thisA = this
    const { invokeEndpointClick } = this.props
    const file = event.currentTarget.files[0]

    var reader = new FileReader()
    reader.onload = function () {
      const state = this.state
      let img = new Image()
      let dimensions
      img.src = reader.result
      img.onload = function() {
        dimensions = {
          'w': img.width,
          'h': img.height
        }
        invokeEndpointClick(file, dimensions).then(function () {
          thisA.setState({
            dimensions: dimensions,
            open: true,
            thumb: reader.result,
          })
        })
      }
    }
    reader.readAsDataURL(file)
  }

  handleCreateLabellingJobClick = (event) => {
    const { handleCreateLabellingJobClick } = this.props
    handleCreateLabellingJobClick()
  }

  handleTrainClick = (event) => {
    const { handleTrainClick } = this.props
    handleTrainClick()
  }

  handleModeChange = (event) => {
  }

  refreshForm = (event) => {
    var r = confirm("It will lost all data, Redo?");
    if (r == true) {
      location.reload()
    }
  }

  getFinalMetricDataList = () => {
    const { finalMetricDataList } = this.props
    if (finalMetricDataList !== null) {

      const dataList = finalMetricDataList.map(function(data) {
        return <div>{data['MetricName']+' : '+Math.round(data['Value'] * 100)+'%'}</div>
      })
      return <div className={'data-list'}>{dataList.slice(0, 2)}</div>
    }
    return ''
  }

  render () {
    const { values, onSubmit, classes, username, isTraining, isReadyToCreateModel, createModelClick, isReadyToInvokeEndpoint, data, handleModeChange, isCreateModelRuning, error, modeDisabled, isReadyToTraining, isReadyToCreateLabelingJob, isCreateLabelingJobRuning, uploadS3Disabled, isReadyToLabel, estimatedTime, clickedCreateLabelingJobTime, clickedStartTrainingTime } = this.props
    const { open } = this.state

    const handleClickOpen = () => {
      this.setState({ open: true })
    }
    const handleClose = () => {
      this.setState({ open: false })
    }

    const initialValues = {
      ...values
    }

    const s3Url = 'https://s3.console.aws.amazon.com/s3/buckets/emsd-821-' + username + '/?region=ap-east-1&tab=overview'
    const labellingUrl = 'https://034oi3dx59.labeling.ap-northeast-1.sagemaker.aws'

    return (
      <Grid
        className={classes.root}
        container
        spacing={0}
        direction='column'
        alignItems='center'
        justify='center'
      >

        <Formik
          initialValues={initialValues}
          onSubmit={onSubmit}
          enableReinitialize
        >
          {({ values, setFieldValue }) => (
            <Form>
              <div>
                <RadioGroup
                  aria-label="mode"
                  name="mode"
                  value={values['mode']}
                  onChange={e => {
                    setFieldValue('mode', e.target.value)
                    handleModeChange(e.target.value)
                  }}
                  className={'radio-group'}
                  row
                >
                  <FormControlLabel value="classification" control={<Radio />} label="Classification" className={'radio-btn'} disabled={modeDisabled} />
                  <FormControlLabel value="detection" control={<Radio />} label="Detection" className={'radio-btn'} disabled={modeDisabled} />
                  <FormControlLabel value="demo" control={<Radio />} label="Demo" className={'radio-btn'} disabled={modeDisabled} />
                  {
                    !isCreateLabelingJobRuning && !isTraining &&
                    <Button variant='contained' size='small' className={classes.button} onClick={this.refreshForm}>Redo</Button>
                  }
                </RadioGroup>
              </div>
              <div>
                <Box width={1200} className={'logo-box'}>
                  <Box mb={3} />
                  <div className={'menu-item'} >
                    {
                      (values['mode'] !== 'demo' && !uploadS3Disabled)
                      ?
                      <a href={s3Url} target='_blank'>
                        <div>
                          <img src={menuIcon01} width='150' />
                        </div>
                        <span>Upload</span>
                      </a>
                      :
                      <div className={'disable-btn'}>
                        <div>
                          <img src={menuIcon01} width='150' />
                        </div>
                        <span>Upload</span>
                      </div>
                    }
                    {
                      (values['mode'] !== 'demo' && isReadyToCreateLabelingJob)
                      ?
                      <div onClick={this.handleCreateLabellingJobClick}>
                        <div>
                          <img src={menuIcon02} width='150' />
                        </div>
                        <span className={'labeling-job'}>Create Labelling Job</span>
                      </div>
                      :
                      <div className={'disable-btn'}>
                        <div>
                          <img src={menuIcon02} width='150' />
                        </div>
                        <span className={'labeling-job'}>Create Labelling Job</span>
                      </div>
                    }
                    {
                      (values['mode'] !== 'demo' && isReadyToLabel)
                      ?
                      <a href={labellingUrl} target='_blank'>
                        <div>
                          <img src={menuIcon02} width='150' />
                        </div>
                        <span>Label</span>
                      </a>
                      :
                      <div className={'disable-btn'}>
                        <div>
                          <img src={menuIcon02} width='150' />
                        </div>
                        <span>Label</span>
                      </div>
                    }
                    {
                      (values['mode'] !== 'demo' && isReadyToTraining)
                      ?
                      <div onClick={this.handleTrainClick}>
                        <div>
                          <img src={menuIcon03} width='150' />
                        </div>
                        <span>Train</span>
                      </div>
                      :
                      <div className={'disable-btn'}>
                        <div>
                          <img src={menuIcon03} width='150' />
                        </div>
                        <span>Train</span>
                      </div>
                    }
                    {
                      (isReadyToInvokeEndpoint || values['mode'] === 'demo')
                        ? <div className={'enable-btn'} onClick={this.handleClick}>
                          <div>
                            <img src={menuIcon04} width='150' />
                            <input accept='image/*' type='file' ref={input => this.inputElement = input} style={{ display: 'none' }} onChange={this.handleFileChange} />
                          </div>
                          <span>Test</span>
                        </div>
                        : <div className={'disable-btn'}>
                          <div>
                            <img src={menuIcon04} width='150' />
                          </div>
                          <span>Test</span>
                        </div>
                    }
                  </div>
                  {
                    !error && isCreateLabelingJobRuning &&
                    <div>
                      <span>Labeling job created. Please allow 3 minutes for labeling job to appear in Ground Truth.</span>
                      <img src={processIcon} width='15' />
                      <br />
                      <span>Estimated time of completion : {estimatedTime}</span>
                    </div>
                  }
                  {
                    !error && isReadyToLabel &&
                    <div>
                      <span>
                        After you have finished labeling, it takes 3 minutes to process the photos, then the "Train" button will be enabled.
                      </span>
                    </div>
                  }
                  {
                    !error &&  isReadyToTraining &&
                    <div>
                      Ready to train. This should take 10 minutes.
                    </div>
                  }
                  {
                    !error && isTraining &&
                    <div>Training job in progress... <img src={processIcon} width='15' /></div>
                  }
                  {
                    // TODO add timestamp
                    isReadyToCreateModel &&
                    <div>
                      <span>Training completed. Deploying trained Model to endpoint.... <img src={processIcon} width='15' /><br />
                      This should take 15 minutes... ETA : {estimatedTime}<br />
                      Once deployed you can test your Model using the "Test" button!
                      </span>
                      <div>
                        Training Accuracy:<br />
                        {
                          this.getFinalMetricDataList()
                        }
                      </div>
                    </div>
                  }
                  {
                    !error && isCreateModelRuning &&
                    <div>Creating Model... <img src={processIcon} width='15' /></div>
                  }
                  {
                    error &&
                    <div className={'error'}>{error}</div>
                  }
                </Box>
              </div>
            </Form>
          )}
        </Formik>

        <Dialog onClose={handleClose} aria-labelledby='customized-dialog-title' open={open}>
          <DialogContent dividers>
            <Typography component={'span'}>
              <div className={'dialog'}>
                <a className={'dialog-image'}>
                  {
                    this.state.thumb &&
                      <div style={{position: 'relative'}}>
                        <Stage width={window.innerWidth} height={window.innerHeight} style={{position: 'absolute'}}>
                          <Layer>
                            {
                              data['rectList'].map(function(rect) {
                                return <Rect
                                  x={rect['x']}
                                  y={rect['y']}
                                  width={rect['w']}
                                  height={rect['h']}
                                  stroke="black"
                                />
                              })
                            }
                          </Layer>
                        </Stage>
                        <img id='thumb-preview' src={this.state.thumb} />
                      </div>
                  }
                </a>
                <div className={'dialog-list'}>
                  <div>
                    <span>{data['classIndex0']['label']}: </span>
                    <span>{data['classIndex0']['confidencePercentage']}</span>
                  </div>
                  <div>
                    <span>{data['classIndex1']['label']}: </span>
                    <span>{data['classIndex1']['confidencePercentage']}</span>
                  </div>
                  <div>
                    <span>{data['classIndex2']['label']}: </span>
                    <span>{data['classIndex2']['confidencePercentage']}</span>
                  </div>
                  <div>
                    <span>{data['classIndex3']['label']}: </span>
                    <span>{data['classIndex3']['confidencePercentage']}</span>
                  </div>
                </div>
              </div>
            </Typography>
          </DialogContent>
          <DialogActions>
              <Button onClick={handleClose} color="primary">
                Close
              </Button>
            </DialogActions>
        </Dialog>
      </Grid>
    )
  }
}

export default Dashboard
