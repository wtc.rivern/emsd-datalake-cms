import * as yup from 'yup'
import React from 'react'
import { inject, observer } from 'mobx-react'
import {
  Grid,
  Typography,
  Box,
  withStyles,
  Button,
  FormHelperText,
} from '@material-ui/core'
import { styled } from '@material-ui/styles';
import { ChevronLeft, ChevronRight } from '@material-ui/icons'
import { TextField } from 'formik-material-ui'
import { Formik, Form, Field } from 'formik'

import logo from '../../images/emsd_logo.png'

const styles = {
  root: {
    // padding: 30
  },
  textField: {
    background: "#BFBFBF",
    borderRadius: 0,
  },
  button: {
    background: "#2FD5B1",
    height: 50,
    borderRadius: 0
  },
  notchedOutline: {
    border: 0
  }
}

@withStyles(styles)

class LoginForm extends React.Component {
  render () {
    const { values, onSubmit, classes } = this.props

    const schemaConfig = {
      username: yup.string().required(),
      password: yup.string().required(),
    }

    const initialValues = {
      ...values
    }

    const validationSchema = yup.object().shape(schemaConfig)

    return (
      <Grid
        className={classes.root}
        container
        spacing={0}
        direction='column'
        alignItems='center'
        justify='center'
      >
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={onSubmit}
          enableReinitialize
        >
          {({ values }) => (
            <Form className={'login'}>
              <div className={'logo'}>
                <a className={'logo-image'}>
                  <img src={logo} width='200' />
                </a>
              </div>
              <div>
                <Box width={320} className={'logo-box'}>
                  <Box mb={3} />
                  <Field
                    name='username'
                    fullWidth
                    component={TextField}
                    variant='filled'
                    InputProps={{
                      classes: {
                        root: classes.textField,
                      }
                    }}
                    placeholder='username'
                  />
                  <Box mb={2} />
                  <Field
                    name='password'
                    fullWidth
                    component={TextField}
                    variant='filled'
                    InputProps={{
                      classes: {
                        root: classes.textField,
                      }
                    }}
                    placeholder='password'
                    type='password'
                  />
                  <Box mb={4} />
                  <Button
                    className={classes.button}
                    type='submit'
                    variant='contained'
                    fullWidth
                    size='large'
                  >
                    {'Login'}
                  </Button>
                </Box>
              </div>
            </Form>
          )}
        </Formik>
      </Grid>
    )
  }
}

export default LoginForm
