import React from 'react'
import { inject, observer } from 'mobx-react'
import {
  withStyles
} from '@material-ui/core'

import Dashboard from '../components/forms/Dashboard'

import logo from '../images/emsd_logo.png'

const styles = {
  root: {
    // height: '100vh'
  },
  primaryText: {
    color: '#2F639E'
  },
  secondaryText: {
    color: '#ccc'
  },
  breadcrumbs: {
    padding: 20,
    paddingLeft: 60
  }
}

@withStyles(styles)
@inject('authStore')
@inject('datalakeStore')
@observer
class Test extends React.Component {

  componentWillMount() {
    const { authStore } = this.props
    authStore.authenticateUser
  }

  handleCreateLabellingJobClick = () => {
    const { datalakeStore } = this.props
    datalakeStore.createLabellingJob()
  }

  handleTrainClick = () => {
    const { datalakeStore } = this.props
    datalakeStore.startTraining()
  }

  createModelClick = () => {
    const { datalakeStore } = this.props
    datalakeStore.testCreateModel()
  }

  invokeEndpointClick = async (image, dimensions) => {
    const { datalakeStore } = this.props
    return await datalakeStore.invokeEndpoint(image, dimensions)
  }

  handleModeChange = async (mode) => {
    const { datalakeStore } = this.props
    return await datalakeStore.changeMode(mode)
  }

  logout = async () => {
    var r = confirm("It will lost all data, Logout?");
    if (r == true) {
      const { authStore } = this.props
      return await authStore.logout()
    }
  }

  render () {
    const { authStore, datalakeStore } = this.props
    const { username } = authStore
    const { isTraining, isReadyToCreateModel, isCreateModelRuning, isReadyToInvokeEndpoint, data, error, modeDisabled, isReadyToTraining, isReadyToCreateLabelingJob, isCreateLabelingJobRuning, uploadS3Disabled, estimatedTime, isReadyToLabel, clickedCreateLabelingJobTime, clickedStartTrainingTime, finalMetricDataList} = datalakeStore
    const { currentForm } = datalakeStore

    return (
      <main id='home' className='dashbroad'>
        <div className={'header'}>
          <div className={'logo'}>
            <a className={'logo-image'}>
              <img src={logo} width='200' />
            </a>
          </div>
          <div className={'logout'} onClick={this.logout}>{'Logout'}</div>
        </div>

        <Dashboard
          username={username}
          handleCreateLabellingJobClick={this.handleCreateLabellingJobClick}
          handleTrainClick={this.handleTrainClick}
          handleTestClick={this.handleTestClick}
          createModelClick={this.createModelClick}
          invokeEndpointClick={this.invokeEndpointClick}
          isTraining={isTraining}
          isReadyToCreateLabelingJob={isReadyToCreateLabelingJob}
          isCreateLabelingJobRuning={isCreateLabelingJobRuning}
          isReadyToTraining={isReadyToTraining}
          isReadyToCreateModel={isReadyToCreateModel}
          isCreateModelRuning={isCreateModelRuning}
          isReadyToInvokeEndpoint={isReadyToInvokeEndpoint}
          data={data}
          values={currentForm}
          handleModeChange={this.handleModeChange}
          error={error}
          modeDisabled={modeDisabled}
          uploadS3Disabled={uploadS3Disabled}
          estimatedTime={estimatedTime}
          isReadyToLabel={isReadyToLabel}
          clickedCreateLabelingJobTime={clickedCreateLabelingJobTime}
          clickedStartTrainingTime={clickedStartTrainingTime}
          finalMetricDataList={finalMetricDataList}
        />
      </main>
    )
  }
}

export default Test
