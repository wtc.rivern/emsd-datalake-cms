import React from 'react'
import { inject, observer } from 'mobx-react'
import {
  withStyles,
} from '@material-ui/core'

import LoginForm from '../components/forms/LoginForm';

const styles = {
  root: {
    // height: '100vh'
  },
  primaryText: {
    color: '#2F639E'
  },
  secondaryText: {
    color: '#ccc'
  },
  breadcrumbs: {
    padding: 20,
    paddingLeft: 60
  },
}

@withStyles(styles)
@inject('authStore')
@observer
class Test extends React.Component {
  handleSubmit = async (values, { setSubmitting, setErrors }) => {

    const { authStore } = this.props
    const { username, password } = values

    await authStore.login(username, password)

    if (authStore.error) {
      setErrors({
        password: authStore.error.message
      })
    }

    setSubmitting(false)
  }

  render () {
    const { authStore } = this.props
    const { currentForm } = authStore

    return (
      <main id='home'>
        <LoginForm
          values={currentForm || undefined}
          onSubmit={this.handleSubmit}
        />
      </main>
    )
  }
}

export default Test
