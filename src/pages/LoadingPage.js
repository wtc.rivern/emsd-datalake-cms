import React from 'react'

import Loading from '../components/layouts/Loading'

const LoadingPage = () => <Loading color='primary' />

export default LoadingPage
