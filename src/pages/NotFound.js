import React from 'react'
import { Grid, Typography, Button, Box, withStyles } from '@material-ui/core'

import { Link } from 'react-router-dom'

const styles = {
  root: {
    height: '100vh'
  }
}

@withStyles(styles)
class NotFound extends React.Component {
  render () {
    const { classes } = this.props
    return (
      <Grid
        className={classes.root}
        container
        spacing={0}
        direction='column'
        alignItems='center'
        justify='center'
      >
        <Typography component='h1' variant='h2'>
          404 Not Found
        </Typography>
        <Box mb={2} />
        <Typography component='h1' variant='body1'>
          The page you are looking for could not be found.
        </Typography>
        <Box mb={5} />
        <Button variant='contained' color='primary' component={Link} to='/'>
          Back to home
        </Button>
      </Grid>
    )
  }
}

export default NotFound
