import { observable, runInAction, toJS } from 'mobx'
import * as api from '../services/api'
import dayjs from 'dayjs'

class DatalakeStore {
  @observable isReadyToCreateLabelingJob = true
  @observable isReadyToLabel = false
  @observable isReadyToTraining = false
  @observable isReadyToCreateModel = false
  @observable isReadyToInvokeEndpoint = false

  @observable isCreateLabelingJobRuning = false
  @observable isTraining = false
  @observable isCreateModelRuning = false
  @observable modeDisabled = false
  @observable currentLabellingJobName = ''
  @observable uploadS3Disabled = false
  @observable clickedCreateLabelingJobTime = ''
  @observable clickedStartTrainingTime = ''
  @observable estimatedTime = ''

  @observable data = {
    classIndex0: {
      'confidencePercentage': '',
      'confidenceScore': '',
      'label': '',
    },
    classIndex1: {
      'confidencePercentage': '',
      'confidenceScore': '',
      'label': '',
    },
    classIndex2: {
      'confidencePercentage': '',
      'confidenceScore': '',
      'label': '',
    },
    classIndex3: {
      'confidencePercentage': '',
      'confidenceScore': '',
      'label': '',
    },
    rectList: []
  }
  @observable finalMetricDataList = null
  @observable currentForm = {
    mode: 'demo',
    dialogMode: 'detection'
  }

  @observable error = null

  constructor (rootStore) {
    this.rootStore = rootStore
  }

  changeMode = async (mode) => {
    this.currentForm.mode = mode
  }

  createLabellingJob = async () => {
    const { accessToken } = this.rootStore.authStore
    const thisA = this
    try {
      this.modeDisabled = true
      this.uploadS3Disabled = true
      this.isReadyToCreateLabelingJob = false
      this.isCreateLabelingJobRuning = true
      this.estimatedTime = dayjs().add(300, "second").format("HH:mm")
      this.clickedCreateLabelingJobTime = dayjs().format("HH:mm")
      api.datalake.createLabellingJob(this.currentForm.mode, accessToken).then(function(createLabellingJobResonseData) {
        if (createLabellingJobResonseData['labellingJobName']) {
          thisA.currentLabellingJobName = createLabellingJobResonseData['labellingJobName']
          setTimeout(function(){
            thisA.estimatedTime = dayjs().add(300, "second").format("HH:mm")
            thisA.isCreateLabelingJobRuning = false
            thisA.isReadyToLabel = true
          }, 300000);
          const createLabellingJobInvervalId = setInterval(function () {
            api.datalake.checkLabellingJobStatus(accessToken, createLabellingJobResonseData['labellingJobName']).then(function (checkLabellingJobStatusResponseData) {
              if (!thisA.isReadyToLabel && checkLabellingJobStatusResponseData['LabelCounters'] !== undefined && checkLabellingJobStatusResponseData['LabelCounters']['Unlabeled'] !== 0) {
                // thisA.estimatedTime = dayjs().add(300, "second").format("HH:mm")
                // thisA.isCreateLabelingJobRuning = false
                // thisA.isReadyToLabel = true
              }
              if (checkLabellingJobStatusResponseData['LabelingJobStatus'] === 'Completed') {
                thisA.isCreateLabelingJobRuning = false
                thisA.isReadyToLabel = false
                thisA.isReadyToTraining = true
                thisA.estimatedTime = dayjs().add(300, "second").format("HH:mm")
                clearInterval(createLabellingJobInvervalId)
              } else if (checkLabellingJobStatusResponseData['LabelingJobStatus'] === 'Failed') {
                thisA.error = 'Labelling Job Failed'
                clearInterval(createLabellingJobInvervalId)
              }
            })
          }, process.env.CHECK_STATUS_API_INTERVAL)
          if (thisA.error) {
            clearInterval(createLabellingJobInvervalId)
          }
        }
      })
    } catch (error) {
      console.log(error)
      this.error = error.error
    }
  }

  checkCreateLabellingJobStatus = async () => {
    const { accessToken } = this.rootStore.authStore
    try {
      return await api.datalake.checkCreateLabellingJobStatus(this.currentForm.mode, accessToken)
    } catch (error) {
      console.log(error)
      runInAction(() => {
        this.error = error.error
      })
    }
  }

  // intervalCheck

  startTraining = async () => {
    const { accessToken } = this.rootStore.authStore
    try {
      console.log(this.currentLabellingJobName)
      await api.datalake.startTraining(this.currentForm.mode, accessToken, this.currentLabellingJobName)
    } catch (error) {
      console.log(error)
      this.error = error.error
    }
    const thisA = this
    this.isReadyToLabel = false
    this.isReadyToTraining = false
    this.isTraining = true
    this.clickedStartTrainingTime = dayjs().format("HH:mm")
    const trainingJobIntervalId = setInterval(function () {
      thisA.checkTrainingJobStatus().then(function (checkTrainingJobStatusResponseData) {
        if (checkTrainingJobStatusResponseData['TrainingJobStatus'] === 'Completed') {
          thisA.isTraining = false
          thisA.isReadyToCreateModel = true
          thisA.estimatedTime = dayjs().add(900, "second").format("HH:mm")
          clearInterval(trainingJobIntervalId)
          thisA.finalMetricDataList = checkTrainingJobStatusResponseData['FinalMetricDataList']
          const createModelIntervalId = setInterval(function () {
            thisA.createModel().then(function(createModelResponseData) {
              console.log(createModelResponseData['success'])
              if (createModelResponseData['success']) {
                clearInterval(createModelIntervalId)
                thisA.isCreateModelRuning = true
                const checkEndpointIntervalId = setInterval(function () {
                  thisA.checkEndpointStatus(accessToken).then(function (checkEndpointStatusResponData) {
                    if (checkEndpointStatusResponData['EndpointStatus'] === 'InService') {
                      thisA.isReadyToCreateModel = false
                      thisA.isCreateModelRuning = false
                      thisA.isReadyToInvokeEndpoint = true
                      clearInterval(checkEndpointIntervalId)
                    }
                  })
                }, process.env.CHECK_STATUS_API_INTERVAL)
              }
            })
          }, process.env.CHECK_STATUS_API_INTERVAL)
        } else if (checkTrainingJobStatusResponseData['TrainingJobStatus'] === 'Failed') {
          this.error = 'Training Job Failed'
          clearInterval(trainingJobIntervalId)
        }
      })
    }, process.env.CHECK_STATUS_API_INTERVAL)
    if (thisA.error) {
      clearInterval(trainingJobIntervalId)
    }
  }

  checkTrainingJobStatus = async () => {
    const { accessToken } = this.rootStore.authStore
    try {
      return await api.datalake.checkTrainingJobStatus(this.currentForm.mode, accessToken)
    } catch (error) {
      console.log(error)
      runInAction(() => {
        this.error = error.error
      })
    }
  }

  createModel = async () => {
    const { accessToken } = this.rootStore.authStore
    try {
      this.isCreateModelRuning = true
      return await api.datalake.createModel(this.currentForm.mode, accessToken)
    } catch (error) {
      console.log(error)
      runInAction(() => {
        this.error = error.error
      })
    }
  }

  checkEndpointStatus = async () => {
    const { accessToken } = this.rootStore.authStore
    try {
      return await api.datalake.checkEndpointStatus(this.currentForm.mode, accessToken)
    } catch (error) {
      console.log(error)
      runInAction(() => {
        this.error = error.error
      })
    }
  }

  invokeEndpoint = async (image, dimensions) => {
    // [class_index, confidence_score, xmin, ymin, xmax, ymax]
    const { accessToken } = this.rootStore.authStore
    console.log(dimensions)
    const scaleRate = dimensions.h / dimensions.w
    try {
      const thisA = this
      await api.datalake.invokeEndpoint(thisA.currentForm.mode, accessToken, image).then(function (result) {
        thisA.currentForm['dialogMode'] = 'detection'
        if (result['result']['prediction'] === undefined) {
          thisA.currentForm['dialogMode'] = 'classification'
        }
        if (thisA.currentForm['dialogMode'] === 'detection') {
          const data = {
            classIndex0: [],
            classIndex1: [],
            classIndex2: [],
            classIndex3: [],
          }
          thisA.data['rectList'] = []
          result['result']['prediction'].map(function (item) {
            data[`classIndex${item[0]}`].push(item)
            if (item[1] > 0.2) {
              thisA.data['rectList'].push({
                x: Math.round(item[2] * 400),
                y: Math.round(item[3] * 400 * scaleRate),
                w: Math.round((item[4] - item[2]) * 400),
                h: Math.round((item[5] - item[3]) * 400 * scaleRate),
              })
            }
          })
          for (var i = 0; i < 4; i++) {
            data[`classIndex${i}`].sort(function compareNumbers (a, b) {
              return a - b
            })
            thisA.data[`classIndex${i}`]['label'] = result['labels'][i]
            if (data[`classIndex${i}`].length > 0) {
              thisA.data[`classIndex${i}`]['confidencePercentage'] = Math.round(data[`classIndex${i}`][0][1] * 100) + '%'
            } else {
              thisA.data[`classIndex${i}`]['confidencePercentage'] = '0%'
            }
          }
        } else {
          thisA.data.classIndex0 = Math.round(result['result'][0] * 100)+'%'
          thisA.data.classIndex1 = Math.round(result['result'][1] * 100)+'%'
          thisA.data.classIndex2 = Math.round(result['result'][2] * 100)+'%'
          thisA.data.classIndex3 = Math.round(result['result'][3] * 100)+'%'
        }
      })
    } catch (error) {
      console.log(error)
      runInAction(() => {
        this.error = error.error
      })
    }
  }
}

export default DatalakeStore
