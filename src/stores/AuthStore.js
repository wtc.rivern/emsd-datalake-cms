import * as R from 'ramda'
import jwt from 'jsonwebtoken'
import ms from 'ms'
import { observable, action, computed, runInAction, reaction } from 'mobx'

import * as api from '../services/api'
import { getTokens, storeTokens, removeTokens } from '../utils/auth'
import history from '../history'

class AuthStore {
  @observable currentForm = {
    username: '',
    password: ''
  }
  @observable isReady = false
  @observable username = null
  @observable accessToken = null
  @observable refreshToken = null
  @observable error = null

  constructor (rootStore) {
    this.rootStore = rootStore

    const { accessToken, refreshToken, username } = getTokens()
    this.accessToken = accessToken
    // this.refreshToken = refreshToken
    this.username = username

    this.setupTokenTimeoutCheck()

    // watch for the change of access token,
    // when it happens, store to local storage
    reaction(
      () => this.accessToken,
      () => {
        // if (this.accessToken && this.refreshToken) {
        if (this.accessToken) {
          storeTokens({
            accessToken: this.accessToken,
            refreshToken: this.refreshToken,
            username: this.username
          })
        } else {
          removeTokens()
        }
      }
    )
  }

  @computed get isAuthenticated () {
    return !!this.accessToken
  }

  @computed get authenticateUser () {
    if (!!this.accessToken === false) {
      alert('Please login first.')
      history.push('/login')
    }
  }

  login = async ( username, password ) => {
    this.clearError()
    try {
      const { accessToken } = await api.auth.login(username, password)
      runInAction(() => {
        this.accessToken = accessToken
        this.username = username
        // this.refreshToken = refreshToken
      })
      // login success
      history.push('/dashboard')
    } catch (error) {
      alert(error.error)
      runInAction(() => {
        this.error = error
      })
    }
  }

  refresh = async () => {
    try {
      const { accessToken, refreshToken } = await api.auth.refresh(this.refreshToken)
      runInAction(() => {
        this.accessToken = accessToken
        this.refreshToken = refreshToken
      })
    } catch (error) {
      // background requests no need to set error
      console.log(error)
    }
  }

  @action
  logout = () => {
    this.accessToken = null
    this.refreshToken = null
    this.username = null
    removeTokens()
    history.push('/login')
  }

  @action
  clearError = () => {
    this.error = null
  }

  refreshIfExpireSoon = async () => {
    if (this.isAuthenticated) {
      const { exp } = jwt.decode(this.accessToken)
      const now = new Date()
      const fiveMinutesFromNow = new Date(now.getTime() + ms('5mins'))
      if (new Date(exp * 1000) <= fiveMinutesFromNow) {
        await this.refresh()
      }
    }
  }

  setupTokenTimeoutCheck = async () => {
    // invoke once immediately
    await this.refreshIfExpireSoon()
    // check access token every 5 minutes,
    // then refresh it if it expires in 5 minutes
    setInterval(this.refreshIfExpireSoon, ms('5mins'))

    runInAction(() => {
      this.isReady = true
    })
  }
}

export default AuthStore
