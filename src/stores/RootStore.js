import AuthStore from './AuthStore'
import DatalakeStore from './DatalakeStore'

class RootStore {
  constructor () {
    this.authStore = new AuthStore(this)
    this.datalakeStore = new DatalakeStore(this)
  }
}

export default RootStore
