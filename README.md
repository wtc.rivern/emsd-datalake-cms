# emsd-datalake-cms

## Install dependencies:
`yarn`

## Setup environment variables:
- Copy `.env.example` to `.env`

## Start development server:
`yarn dev`

## Production build:
`yarn build`
